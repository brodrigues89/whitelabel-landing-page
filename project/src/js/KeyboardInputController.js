
var KeyboardInputController = {

    initialise: function () {
        KeyboardInputController.initKeyboardControls();
        KeyboardInputController.hideOutlines();
    },

    hideOutlines: function () {
        $('a[href], area[href], input, select, textarea, button, iframe, object, embed, *[tabindex], *[contenteditable], .timeline-control-button')
            .not('[disabled], :hidden').removeClass('focusable').addClass('no-focus');
    },

    showOutlines: function () {
        $('a[href], area[href], input, select, textarea, button, iframe, object, embed, *[tabindex], *[contenteditable], .timeline-control-button')
            .not('[disabled], :hidden').removeClass('no-focus').addClass('focusable');
    },

    initKeyboardControls: function() {
        var keyStart = {37: null, 39: null};
        var keyEnd = {37: null, 39: null};

        //Add a listener on the body of videoPlayer.php
        $(document).keydown(function(e) {
            //Then, if the element in focus isn't an input, select, textarea or form, allow the user to control the video player
            var inputActive = $("input, select, textarea, form").is(":focus");
            var currentDate = new Date();

            if(inputActive === false) {
                switch (e.keyCode) {
                    //Space bar
                    case 32:
                        e.preventDefault();
                        if (VideoPlayerInterface.iframeWindow.rtc.player.video.status().paused) {
                            VideoPlayerInterface.iframeWindow.rtc.player.controls.resume();
                        } else {
                            VideoPlayerInterface.iframeWindow.rtc.player.controls.pause();
                        }

                        VideoPlayerInterface.iframeWindow.rtc.utils.track("keyboard.spacebar");
                        break;
                    //Left and right arrow keys
                    case 37:
                    case 39:
                        //Record the time the keypress started
                        if(keyStart[e.keyCode] === null) {
                            keyStart[e.keyCode] = new Date();
                        }

                        //Then if the current time is a second after the key press started, rewind or fast-forward the video
                        if(currentDate.getTime() > keyStart[e.keyCode].getTime() + 500) {
                            if(VideoPlayerInterface.iframeWindow.rtc.player.vars.videoDuration !== 0 && VideoPlayerInterface.iframeWindow.rtc.player.vars.videoDuration < 10) {
                                if(e.keyCode == 37) {
                                    VideoPlayerInterface.iframeWindow.rtc.player.skipPrevious();
                                    VideoPlayerInterface.iframeWindow.rtc.utils.track("keyboard.skip-previous");
                                } else {
                                    VideoPlayerInterface.iframeWindow.rtc.player.skipNext();
                                    VideoPlayerInterface.iframeWindow.rtc.utils.track("keyboard.skip-next");
                                }
                            } else if(VideoPlayerInterface.iframeWindow.rtc.player.vars.videoDuration !== 0) {
                                if(e.keyCode == 37) {
                                    VideoPlayerInterface.iframeWindow.rtc.player.vars.currentTime -= 10;
                                    VideoPlayerInterface.iframeWindow.rtc.utils.track("keyboard.rewind", "newTime=" + rtc.player.vars.currentTime);
                                } else {
                                    VideoPlayerInterface.iframeWindow.rtc.player.vars.currentTime += 10;
                                    VideoPlayerInterface.iframeWindow.rtc.utils.track("keyboard.fast-forward", "newTime=" + rtc.player.vars.currentTime);
                                }

                                //Limiting the percentage to 99.9% as 100% seems to break the rewind/fast-forward functionality
                                var percent = Math.min((VideoPlayerInterface.iframeWindow.rtc.player.vars.currentTime / VideoPlayerInterface.iframeWindow.rtc.player.vars.videoDuration) * 100, 99.9);
                                VideoPlayerInterface.iframeWindow.rtc.timeline.slideCurrentState(RTCVisit.currentState, null, percent);
                            }
                        }
                        break;
                    case 13:
                        if(document.activeElement === $('#jsQualitySelectorButtonIcon')[0]){
                            $('#jsQualitySelectorPopout').toggle();
                        } else if (document.activeElement.type !== 'button'){
                            $(document.activeElement).click();
                        }
                        break;
                    case 9:
                        KeyboardInputController.showOutlines();
                        break;
                }
            }
        });

        $(document).keyup(function(e) {
            //If the element in focus isn't an input, select, textarea or form, allow the user to control the video player
            var inputActive = $("input, select, textarea, form").is(":focus");
            var currentDate = new Date();

            if(inputActive === false) {
                switch (e.keyCode) {
                    //Left and right arrow keys
                    case 37:
                    case 39:
                        keyStart[e.keyCode] = null;

                        //If the user has pressed an arrow key twice, skip the section
                        if(keyEnd[e.keyCode] !== null) {
                            if(e.keyCode == 37 && currentDate.getTime() < keyEnd[37].getTime() + 1000) {
                                VideoPlayerInterface.iframeWindow.rtc.player.skipPrevious();
                                VideoPlayerInterface.iframeWindow.rtc.utils.track("keyboard.skip-previous");
                            } else if(e.keyCode == 39 && currentDate.getTime() < keyEnd[39].getTime() + 1000) {
                                VideoPlayerInterface.iframeWindow.rtc.player.skipNext();
                                VideoPlayerInterface.iframeWindow.rtc.utils.track("keyboard.skip-next");
                            }
                        }

                        //Record the time the keyup started
                        if(keyEnd[e.keyCode] === null || currentDate.getTime() >= keyEnd[e.keyCode].getTime() + 1000) {
                            keyEnd[e.keyCode] = new Date();
                        }
                        break;
                }
            }
        });
    }
};